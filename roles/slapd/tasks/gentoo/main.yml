---
- name: install packages
  portage:
    name: "{{ item }}"
    getbinpkg: yes
    quiet: yes
  with_items:
    - net-nds/openldap

- name: add ldap user to group ssl
  user:
    name: ldap
    groups: ssl
    append: yes
  notify:
    - restart slapd

- name: hash slapd mdb password
  command: bash -c "slappasswd -h '{CRYPT}' -s '{{ slapdConfig['db']['mdb']['password'] }}' > /etc/openldap/.slapdDbMdbPasswordHash"
  args:
    creates: /etc/openldap/.slapdDbMdbPasswordHash
  changed_when: false

- name: hash slapd config password
  command: bash -c "slappasswd -h '{CRYPT}' -s '{{ slapdConfig['db']['config']['password'] }}' > /etc/openldap/.slapdDbConfigPasswordHash"
  args:
    creates: /etc/openldap/.slapdDbConfigPasswordHash
  changed_when: false

- name: read mdb password hash
  command: cat /etc/openldap/.slapdDbMdbPasswordHash
  register: slapdDbMdbPasswordHash
  changed_when: false

- name: read config password hash
  command: cat /etc/openldap/.slapdDbConfigPasswordHash
  register: slapdDbConfigPasswordHash
  changed_when: false

- name: create /etc/openldap/slapd.conf
  template:
    src: gentoo/etc/openldap/slapd.conf.j2
    dest: /etc/openldap/slapd.conf
    owner: root
    group: ldap
    mode: 0640

- name: create /etc/openldap/schema/mail.schema
  file:
    src: gentoo/etc/openldap/schema/mail.schema
    dest: /etc/openldap/schema/mail.schema
    owner: root
    group: ldap
    mode: 0640
    state: file

- name: create /etc/conf.d/slapd
  copy:
    src: gentoo/etc/conf.d/slapd.j2
    dest: /etc/conf.d/slapd
    owner: root
    group: root
    mode: 0640
  notify:
    - restart slapd

- name: create directories
  file:
    dest: "{{ item.dest }}"
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    mode: "{{ item.mode }}"
    state: directory
  with_items:
    - { dest: /etc/openldap/slapd.d, owner: ldap, group: ldap, mode: 750 }
    - { dest: /var/lib/slapd, owner: ldap, group: ldap, mode: 750 }

- name: create slapd database
  command: "bash -c 'echo | slapadd -f /etc/openldap/slapd.conf'"
  args:
    creates: /var/lib/slapd/data.mdb

- name: create slapd.d directory
  command: slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d
  args:
    creates: /etc/openldap/slapd.d/cn=config.ldif

- name: set permissions
  file:
    dest: "{{ item.dest }}"
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    recurse: yes
    state: directory
  with_items:
    - { dest: /etc/openldap/slapd.d, owner: ldap, group: ldap }
    - { dest: /var/lib/slapd, owner: ldap, group: ldap }

- name: link initscript
  file:
    src: /etc/init.d/slapd
    dest: /etc/runlevels/default/slapd
    state: link

- name: start service
  service:
    name: slapd
    state: started
