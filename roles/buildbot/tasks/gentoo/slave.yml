---
- name: install package
  portage:
    name: "{{ item }}"
    getbinpkg: yes
    quiet: yes
  with_items:
    - dev-vcs/git
    - dev-util/buildbot-slave

- name: link initscript
  file:
    src: /etc/init.d/buildslave
    dest: /etc/runlevels/default/buildslave
    state: link

- name: update user
  user:
    name: buildbot
    home: /var/lib/buildslave

- name: read user home directory
  user_info:
    user: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
  register: userInfo

- name: update ssh config
  lineinfile:
    dest: "{{ (userInfo['user_info'] | default({}))['home'] | default('/var/lib/buildslave') }}/.ssh/config"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    owner: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
    group: "{{ buildbotConfig['slave']['group'] | default('buildbot') }}"
    mode: 0600
    state: present
    create: yes
  with_items:
    - { regexp: '^StrictHostKeyChecking .*$', line: StrictHostKeyChecking no }
    - { regexp: '^IdentityFile .*$', line: IdentityFile ~/.ssh/build.key }

- name: copy private key
  copy:
    content: "{{ authConfig['sshKey'] }}"
    dest: "{{ (userInfo['user_info'] | default({}))['home'] | default('/var/lib/buildslave') }}/.ssh/build.key"
    owner: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
    group: "{{ buildbotConfig['slave']['group'] | default('buildbot') }}"
    mode: 0600

- name: create data directory
  file:
    dest: /var/lib/buildslave
    owner: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
    group: "{{ buildbotConfig['slave']['group'] | default('buildbot') }}"
    mode: 0750
    state: directory

- name: create buildslave
  command: "buildslave create-slave /var/lib/buildslave {{ buildbotConfig['slave']['master'] }}:9989 {{ buildbotConfig['slave']['name'] }} {{ buildbotConfig['slave']['password'] }}"
  args:
    creates: /var/lib/buildslave/info
  register: buildCreate

- name: update permissions
  file:
    dest: /var/lib/buildslave
    owner: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
    group: "{{ buildbotConfig['slave']['group'] | default('buildbot') }}"
    recurse: yes
  when: buildCreate.changed

- name: update metadata
  template:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "{{ buildbotConfig['slave']['user'] | default('buildbot') }}"
    group: "{{ buildbotConfig['slave']['group'] | default('buildbot') }}"
    mode: 0640
  with_items:
    - { src: gentoo/var/lib/buildslave/info/admin.j2, dest: /var/lib/buildslave/info/admin }
    - { src: gentoo/var/lib/buildslave/info/host.j2, dest: /var/lib/buildslave/info/host }
  notify:
    - restart buildslave

- name: update buildbot settings
  lineinfile:
    dest: /var/lib/buildslave/buildbot.tac
    regexp: '^umask = .*$'
    line: umask = 022
    state: present

- name: update config
  lineinfile:
    dest: "{{ item.dest }}"
    regexp: "{{ item.regexp }}"
    line: "{{ item.line }}"
    state: present
  vars:
    buildbotName: "{{ (buildbotConfig['slave'] | default({}))['name'] | default('') }}"
    buildbotPassword: "{{ (buildbotConfig['slave'] | default({}))['password'] | default('') }}"
    buildbotUser: "{{ (buildbotConfig['slave'] | default({}))['user'] | default('buildbot') }}"
  with_items:
    - { dest: /var/lib/buildslave/buildbot.tac, regexp: '^slavename = .*', line: "slavename = '{{ buildbotName }}'" }
    - { dest: /var/lib/buildslave/buildbot.tac, regexp: '^passwd = .*', line: "passwd = '{{ buildbotPassword }}'" }
    - { dest: /etc/conf.d/buildslave, regexp: '^USERNAME=".*"', line: "USERNAME=\"{{ buildbotUser }}\"" }
  notify:
    - restart buildslave

- name: set python default version
  command: eselect python set 1

- name: start service
  service:
    name: buildslave
    state: started

- name: include slave profile (ansible)
  include: slaves/ansible.yml
  when: buildbotConfig['slave']['profiles']['ansible'] is defined

- name: include slave profile (gentoo-build)
  include: slaves/gentoo-build.yml
  when: buildbotConfig['slave']['profiles']['gentoo-build'] is defined
